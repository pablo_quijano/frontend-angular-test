# Finerio Angular Test App

Aplicación de prueba con Apis de Finerio, desarrollado con Angular

### Pre-requisitos

1. [Node.js](https://nodejs.org/es/)
2. [Angular CLI](https://angular.io/guide/setup-local)
## Instalación
```bash
npm install
```
Instala las dependencias del proyecto

## Ejecución

```bash
ng serve --open
```

La aplicación se ejecutará en modo desarrollador.\
Abre [http://localhost:4200](http://localhost:4200) en el navegador para visualizar la aplicación.