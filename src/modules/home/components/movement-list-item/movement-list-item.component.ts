import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'home-movement-list-item',
  templateUrl: './movement-list-item.component.html',
  styleUrls: ['./movement-list-item.component.sass'],
})
export class MovementListItemComponent implements OnInit {
  
  @Input()
  amount: string;
  @Input()
  date: string;
  @Input()
  description: string;
  @Input()
  type: string;

  constructor() {}

  ngOnInit(): void {}
}
