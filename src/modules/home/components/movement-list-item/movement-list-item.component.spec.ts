import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MovementListItemComponent } from './movement-list-item.component';

describe('MovementListItemComponent', () => {
  let component: MovementListItemComponent;
  let fixture: ComponentFixture<MovementListItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MovementListItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MovementListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
