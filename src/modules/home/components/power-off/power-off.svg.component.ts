import { Component } from '@angular/core';

@Component({
  selector: 'power-off-svg',
  templateUrl: './power-off.svg',
  styleUrls: ['./power-off.svg.component.sass'],
})
export class PowerOffSvgComponent {}
