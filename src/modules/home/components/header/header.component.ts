import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from '@services/auth.service';

@Component({
  selector: 'home-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit {

  @Input()
  name: string;

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
  }

  handleLogOut(): void {
    this.authService.logOut();
  }

}
