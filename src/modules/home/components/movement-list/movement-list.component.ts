import { Component, Input, OnInit } from '@angular/core';
import { Movement } from '@models/movement';
import { MovementsResponse } from '@interfaces/movementsResponse';

import { MovementsService } from '@services/movements.service';
import { Constants } from '@shared/constants';

@Component({
  selector: 'home-movement-list',
  templateUrl: './movement-list.component.html',
  styleUrls: ['./movement-list.component.sass'],
})
export class MovementListComponent implements OnInit {
  currentPage: number;
  loadMore: boolean;
  movements: Movement[];

  @Input()
  userId: string;

  constructor(private movementsService: MovementsService) {}

  ngOnInit(): void {
    this.movements = [];
    this.currentPage = 0;
    this.loadMore = true;
    this.fetchMovements();
  }

  fetchMovements() {
    this.movementsService
      .getMovements(this.userId, this.currentPage, Constants.MOVEMENTS_BY_PAGE)
      .subscribe({
        next: this.handleMovementApiSuccess.bind(this),
        error: this.handleMovementApiFailure.bind(this),
      });
  }

  onScroll() {
    if (this.loadMore) {
      this.fetchMovements();
    }
  }

  handleMovementApiFailure(): void {
    this.loadMore = false;
  }

  handleMovementApiSuccess(response: MovementsResponse): void {
    if (response.data.length) {
      this.currentPage += Constants.MOVEMENTS_BY_PAGE;
      this.movements = [
        ...this.movements,
        ...response.data.map((movementData) => new Movement(movementData)),
      ];
    } else {
      this.loadMore = false;
    }
  }
}
