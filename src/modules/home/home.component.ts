import { Component, OnInit } from '@angular/core';
import { Me } from '@models/me';

import { MeService } from '@services/me.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass'],
})
export class HomeComponent implements OnInit {
  me: Me;
  constructor(private meService: MeService) {}

  ngOnInit(): void {
    this.me = new Me();
    this.meService.getMe().subscribe({
      next: this.handleMeApiSuccess.bind(this),
    });
  }

  handleMeApiSuccess(data: any): void {
    this.me.setData(data);
  }
}
