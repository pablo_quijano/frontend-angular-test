import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Auth } from '@models/auth';
import { AuthService } from '@services/auth.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.sass'],
})
export class FormComponent implements OnInit {
  authHasError: boolean;
  isSubmitting: boolean;
  loginForm: FormGroup;

  constructor(private authService: AuthService) {}

  ngOnInit(): void {
    this.isSubmitting = false;
    this.loginForm = new FormGroup({
      username: new FormControl({ value: '', disabled: this.isSubmitting }, [
        Validators.required,
        Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
      ]),
      password: new FormControl({ value: '', disabled: this.isSubmitting }, [
        Validators.required,
      ]),
    });
    this.authHasError = false;
  }

  get username() {
    return this.loginForm.controls['username'];
  }

  get password() {
    return this.loginForm.controls['password'];
  }

  handleAuthSuccess(data: any): void {
    this.isSubmitting = false;
    if (data && data.access_token && data.token_type) {
      this.authService.authAcces(data.access_token, data.token_type);
    }
  }

  handleAuthFailure(): void {
    this.authHasError = true;
    this.isSubmitting = false;
  }

  onSubmit(authData: Auth): void {
    if (!this.loginForm.valid) {
      return;
    }
    if (this.authHasError) {
      this.authHasError = false;
    }
    const auth = new Auth(authData);
    this.isSubmitting = true;
    this.authService.logIn(auth).subscribe({
      next: this.handleAuthSuccess.bind(this),
      error: this.handleAuthFailure.bind(this),
    });
  }
}
