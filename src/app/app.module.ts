import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from '@modules/login/login.component';
import { HomeComponent } from '@modules/home/home.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { FormComponent } from '@modules/login/components/form/form.component';
import { APIInterceptor } from '@services/APIInterceptor';
import { HeaderComponent } from '@modules/home/components/header/header.component';
import { MovementListItemComponent } from '@modules/home/components/movement-list-item/movement-list-item.component';
import { PowerOffSvgComponent } from '@modules/home/components/power-off/power-off.svg.component';
import { MovementListComponent } from '@modules/home/components/movement-list/movement-list.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    NotFoundComponent,
    FormComponent,
    HeaderComponent,
    MovementListItemComponent,
    PowerOffSvgComponent,
    MovementListComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    InfiniteScrollModule,
    ReactiveFormsModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: APIInterceptor, multi: true },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
