import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MeService } from './me.service';

describe('MeService', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [MeService],
    });
  });

  it('should be initialized', inject(
    [MeService],
    (service: MeService) => {
      expect(service).toBeTruthy();
    }
  ));
});
