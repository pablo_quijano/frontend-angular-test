import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MovementsService } from './movements.service';

describe('MovementsService', () => {
  let service: MovementsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [MovementsService],
    });
    service = TestBed.inject(MovementsService);
  });

  it('should be initialized', inject(
    [MovementsService],
    (service: MovementsService) => {
      expect(service).toBeTruthy();
    }
  ));
});
