import { TestBed, inject } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthService } from './auth.service';
import { Auth } from '@models/auth';

describe('AuthService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule],
      providers: [AuthService],
    });
  });

  it('should be initialized', inject(
    [AuthService],
    (authService: AuthService) => {
      expect(authService).toBeTruthy();
    }
  ));

  it('should perform login correctly', inject(
    [AuthService, HttpTestingController],
    (authService: AuthService, backend: HttpTestingController) => {
      const user = new Auth('test@example.com', 'testpassword');
      authService.logIn(user).subscribe({
        next: (data: any) => {
          expect(data.success).toBe(true);
          expect(data.message).toBe('login was successful');
        },
        error: (_error: any) => {},
      });

      backend
        .expectOne({
          url: '/login',
        })
        .flush({
          success: true,
          message: 'login was successful',
        });
    }
  ));

  it('should fail login correctly', inject(
    [AuthService, HttpTestingController],
    (authService: AuthService, backend: HttpTestingController) => {
      const auth = new Auth('test@example.com', 'wrongPassword');
      authService.logIn(auth).subscribe({
        next: (data: any) => {
          expect(data.success).toBe(false);
          expect(data.message).toBe('email and password combination is wrong');
        },
        error: (_error: any) => {},
      });

      backend
        .expectOne({
          url: '/login',
        })
        .flush({
          success: false,
          message: 'email and password combination is wrong',
        });
    }
  ));
});
