import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

import { Auth } from '@models/auth';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient, private router: Router) {}

  authAcces(accessToken: string, tokenType: string): void {
    if (accessToken && tokenType) {
      localStorage.setItem('access_token', accessToken);
      localStorage.setItem('token_type', tokenType);
      this.router.navigate(['/']);
    }
  }

  getSessionToken(): string {
    const accessToken = localStorage.getItem('access_token');
    const tokenType = localStorage.getItem('token_type');
    if (!accessToken || !tokenType) {
      return '';
    }
    return `${tokenType} ${accessToken}`;
  }

  isAuthenticated(): boolean {
    const sessionToken = this.getSessionToken();
    return sessionToken !== '';
  }

  logIn(auth: Auth): Observable<any> {
    return this.http.post('/login', auth.toJson());
  }

  logOut(): void {
    localStorage.clear();
    this.router.navigate(['/login']);
  }
}
