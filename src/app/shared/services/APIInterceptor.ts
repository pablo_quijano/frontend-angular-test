import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpErrorResponse,
  HttpResponse,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
} from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '@environments/environment';
import { AuthService } from '@services/auth.service';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class APIInterceptor implements HttpInterceptor {
  constructor(private router: Router, private authService: AuthService) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const APIEndpoint = environment.APIEndpoint;
    const apiReq = req.clone({
      url: `${APIEndpoint}${req.url}`,
      setHeaders: { Authorization: this.authService.getSessionToken() },
    });
    return next.handle(apiReq).pipe(
      catchError((err: any) => {
        if (err.status === 401 && this.router.url !== '/login') {
          this.authService.logOut();
        }
        throw new HttpErrorResponse(err);
      })
    );
  }
}
