import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class MovementsService {
  constructor(private http: HttpClient) {}

  getMovements(userId: string, offset: number, max: number): Observable<any> {
    return this.http.get(
      `/users/${userId}/movements?deep=true&offset=${offset}&max=${max}&includeCharges=true&includeDeposits=true&includeDuplicates=true`
    );
  }
}
