import { MovementInterface } from '../models/movement';

export interface MovementsResponse {
  data: MovementInterface[];
  size: number;
}
