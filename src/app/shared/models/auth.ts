export interface AuthInterface {
  username: string;
  password: string;
}
export class Auth implements AuthInterface {
  private _username: string;
  private _password: string;

  constructor({ username, password }: { username: string; password: string }) {
    this._username = username;
    this._password = password;
  }

  public get username() {
    return this._username;
  }

  public set username(value) {
    this._username = value;
  }

  public get password() {
    return this._password;
  }

  public set password(value) {
    this._password = value;
  }

  public toJson() {
    return {
      username: this._username,
      password: this._password,
    };
  }
}
