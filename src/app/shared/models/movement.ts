export interface MovementInterface {
  amount: number;
  date: string;
  description: string;
  id: string;
  type: string;
}

export class Movement implements MovementInterface {
  private _amount: number;
  private _date: string;
  private _description: string;
  private _formattedAmount: string;
  private _formattedDate: string;
  private _id: string;
  private _type: string;

  public get amount() {
    return this._amount;
  }

  public set amount(value) {
    this._amount = value;
  }

  public get date() {
    return this._date;
  }

  public set date(value) {
    this._date = value;
  }

  public get description() {
    return this._description;
  }

  public set description(value) {
    this._description = value;
  }

  public get formattedAmount() {
    return this._formattedAmount;
  }

  public get formattedDate() {
    return this._formattedDate;
  }

  public get id() {
    return this._id;
  }

  public set id(value) {
    this._id = value;
  }

  public get type() {
    return this._type;
  }

  public set type(value) {
    this._type = value;
  }

  constructor({
    amount,
    date,
    description,
    id,
    type,
  }: {
    amount: number;
    date: string;
    description: string;
    id: string;
    type: string;
  }) {
    this._amount = amount;
    this._date = date;
    this._description = description;
    this._formattedAmount = new Intl.NumberFormat('es-MX', {
      style: 'currency',
      currency: 'MXN',
      minimumFractionDigits: 0,
      maximumFractionDigits: 0,
    }).format(amount);
    this._formattedDate = new Intl.DateTimeFormat('es-MX').format(
      new Date(date)
    );
    this._id = id;
    this._type = type;
  }
}
