export interface MeInterface {
  id: string;
  name: string;
}

export class Me implements MeInterface {
  private _id: string = '';
  private _name: string = '';

  public get id() {
    return this._id;
  }

  public set id(value) {
    this._id = value;
  }

  public get name() {
    return this._name;
  }

  public set name(value) {
    this._name = value;
  }

  public setData({ id, name }: { id: string; name: string }) {
    this._id = id;
    this._name = name;
  }
}
